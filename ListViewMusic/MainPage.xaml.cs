﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ListViewMusic
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            CreateList();
        }

        private void CreateList()
        {
            var DaList = new ObservableCollection<MyCell1>();

            var mycell01 = new MyCell1
            {
                word = "teststtstststs",
                pic = ImageSource.FromFile("IMG_3368.png"),

            };

            var mycell02 = new MyCell1
            {
                word = "tesrgfaraefststs",
                pic = ImageSource.FromFile("IMG_3368.png"),
            };

            DaList.Add(mycell01);
            DaList.Add(mycell02);

            MusicListView.ItemsSource = DaList;

        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new MyPage());
        }

        void Handle_Clicked_1(object sender, System.EventArgs e)
        {
        }
    }
}